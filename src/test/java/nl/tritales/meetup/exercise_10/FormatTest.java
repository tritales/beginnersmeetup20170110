package nl.tritales.meetup.exercise_10;

import static org.junit.Assert.assertEquals;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;
import org.junit.Test;

/**
 * Exercise 10: Format the money object so that the test passes
 */
public class FormatTest {

    @Test
    public void format()  {
    	MonetaryAmount amount = Money.of(10.99,"EUR");
    	
    	String formattedString = amount.toString();

    	assertEquals("€ 10,99", formattedString);
    }

}
