package nl.tritales.meetup.exercise_09;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.convert.ConversionQuery;
import javax.money.convert.ConversionQueryBuilder;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;
import javax.money.convert.MonetaryConversions;

import org.javamoney.moneta.Money;
import org.javamoney.moneta.convert.ExchangeRateType;
import org.junit.Test;

/**
 * Exercise 9: Convert euros to dollars using the exchange rate at a given date
 * - What is wrong with this code?
 * - How could you correct it?
 */
public class ExchangeRateTest {

    @Test
    public void exchangeRate()  {
    	MonetaryAmount money = Money.of(10,"EUR");
    	CurrencyUnit dollar = Monetary.getCurrency("USD");

    	ExchangeRateProvider provider = MonetaryConversions.getExchangeRateProvider(ExchangeRateType.IMF_HIST);

    	LocalDate date = Year.of(2017).atMonth(Month.JANUARY).atDay(8);
    	ConversionQuery query = ConversionQueryBuilder.of().setTermCurrency(dollar).set(date).build();
    	CurrencyConversion currencyConversion = provider.getCurrencyConversion(query);
    	MonetaryAmount result = currencyConversion.apply(money);
    	
    	assertEquals("USD 10.588967", result.toString());
    }

}
