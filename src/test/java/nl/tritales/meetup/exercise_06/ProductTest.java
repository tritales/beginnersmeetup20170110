package nl.tritales.meetup.exercise_06;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Exercise 6 Replace your Currency enum by java.util.Currency
 */
public class ProductTest {

    private Product a = new Product("A", new BigDecimal("1.03"), Currency.EURO);
    private Product b = new Product("B", new BigDecimal(".42"), Currency.DOLLAR);

    @Test
    public void substract() throws Exception {
        BigDecimal result = a.getAmount().subtract(b.getAmount());
        assertEquals(new BigDecimal("0.61"), result);
    }

    @Test
    public void currency() throws Exception {
        assertSame(Currency.EURO, a.getCurrency());
        assertSame(Currency.DOLLAR, b.getCurrency());
    }

}