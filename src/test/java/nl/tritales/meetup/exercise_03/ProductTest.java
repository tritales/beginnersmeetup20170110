package nl.tritales.meetup.exercise_03;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Excercise 3: rewrite the code to use java.math.BigDecimal
 */
public class ProductTest {

    @Test
    public void substract() throws Exception {
        Product a = new Product(1.03);
        Product b = new Product(.42);

        double result = a.getAmount() - b.getAmount();
        assertSame(0.61, result);
    }

}