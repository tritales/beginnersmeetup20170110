package nl.tritales.meetup.exercise_05;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Exercise 5 Make currency type-safe by introducing an enum.
 */
public class ProductTest {

    private Product a = new Product("A", new BigDecimal("1.03"), "EURO");
    private Product b = new Product("B", new BigDecimal(".42"), "DOLLAR");

    @Test
    public void substract() throws Exception {
        BigDecimal result = a.getAmount().subtract(b.getAmount());
        assertEquals(new BigDecimal("0.61"), result);
    }

    @Test
    public void currency() throws Exception {
        assertEquals("EURO", a.getCurrency());
        assertEquals("DOLLAR", b.getCurrency());
    }

}