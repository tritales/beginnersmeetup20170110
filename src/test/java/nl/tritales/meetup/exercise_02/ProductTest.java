package nl.tritales.meetup.exercise_02;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Excercise 2: rewrite the code to use a long instead of a double
 */
public class ProductTest {

    @Test
    public void subtract() throws Exception {
        Product a = new Product(1.03);
        Product b = new Product(.42);

        double result = a.getAmount() - b.getAmount();
        assertSame(0.61, result);
    }

}