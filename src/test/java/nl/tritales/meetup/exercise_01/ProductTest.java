package nl.tritales.meetup.exercise_01;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Excercise 1: represent an amount using primitive floating types
 */
public class ProductTest {

    @Test
    public void subtract() throws Exception {
        Product a = new Product(1.03);
        Product b = new Product(.42);

        double result = a.getAmount() - b.getAmount();
        assertSame(0.61, result);
    }

}