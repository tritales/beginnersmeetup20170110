package nl.tritales.meetup.exercise_04;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Exercise 4 Add a field of type String to the Product class and name it currency
 */
public class ProductTest {

    private Product a = new Product("A", new BigDecimal("1.03"));
    private Product b = new Product("B", new BigDecimal(".42"));

    @Test
    public void substract() throws Exception {
        BigDecimal result = a.getAmount().subtract(b.getAmount());
        assertEquals(new BigDecimal("0.61"), result);
    }

    @Test
    public void currency() throws Exception {
//        assertEquals("EUR", a.getCurrency());
//        assertEquals("USD", a.getCurrency());
    }

}