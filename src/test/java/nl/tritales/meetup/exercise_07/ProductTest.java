package nl.tritales.meetup.exercise_07;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.Assert.*;

/**
 * Exercise 7 Introduce a Money class
 */
public class ProductTest {

    private Product a = new Product("A", new BigDecimal("1.03"), Currency.getInstance("EUR"));
    private Product b = new Product("B", new BigDecimal(".42"), Currency.getInstance("USD"));

    @Test
    public void substract() throws Exception {
        BigDecimal result = a.getAmount().subtract(b.getAmount());
        assertEquals(new BigDecimal("0.61"), result);
    }

    @Test
    public void currency() throws Exception {
        assertSame(Currency.getInstance("EUR"), a.getCurrency());
        assertSame(Currency.getInstance("USD"), b.getCurrency());
    }
}