package nl.tritales.meetup.exercise_11;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;
import org.junit.Test;

/**
 * Exercise 11
 */
public class StreamsTest {

	private MonetaryAmount[] amounts = { Money.of(9, "EUR"), Money.of(10, "EUR"), Money.of(7.8, "EUR"), Money.of(9.99, "EUR"), Money.of(49.99, "EUR") };
	
	@Test
	public void sortByValue() throws Exception {
		// Sort the amounts array by value
	}

	@Test
	public void calculateAverage() throws Exception {
		// Calculate the average value of the amount held in the amounts array
	}

	@Test
	public void filter() throws Exception {
		// Filter out all the amount with a value lower than € 10,00
	}

}
