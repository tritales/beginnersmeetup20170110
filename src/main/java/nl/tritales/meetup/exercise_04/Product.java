package nl.tritales.meetup.exercise_04;

import java.math.BigDecimal;

public class Product {

    private final String name;
    private final BigDecimal amount;

    public Product(String name, BigDecimal amount) {
        this.name = name;
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

}
