package nl.tritales.meetup.exercise_06;

import java.math.BigDecimal;

public class Product {

    private final String name;
    private final BigDecimal amount;
    private final Currency currency;

    public Product(String name, BigDecimal amount, Currency currency) {
        this.name = name;
        this.amount = amount;
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public Currency getCurrency() {
        return currency;
    }

}
