package nl.tritales.meetup.exercise_06;

public enum Currency {

    EURO, DOLLAR, POUND, YEN;

}
