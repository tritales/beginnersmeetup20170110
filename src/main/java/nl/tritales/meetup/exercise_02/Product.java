package nl.tritales.meetup.exercise_02;

public class Product {

    private final double amount;

    public Product(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

}
