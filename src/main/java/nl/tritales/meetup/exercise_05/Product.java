package nl.tritales.meetup.exercise_05;

import java.math.BigDecimal;

public class Product {

    private final String name;
    private final BigDecimal amount;
    private final String currency;

    public Product(String name, BigDecimal amount, String currency) {
        this.name = name;
        this.amount = amount;
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public String getCurrency() {
        return currency;
    }

}
